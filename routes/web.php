<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('cliente', 'App\Http\Controllers\ClienteController');
Route::resource('eventos', 'App\Http\Controllers\EventoController');
Route::resource('/catalogos', 'App\Http\Controllers\CatalogoController');
Route::resource('/extras', 'App\Http\Controllers\ExtrasController');