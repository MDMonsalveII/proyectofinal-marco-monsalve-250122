<?php

namespace App\Http\Controllers;

use App\Models\catalogo;
use App\Models\cobro;
use App\Models\direccion;
use App\Models\evento;
use App\Models\tipoPago;
use Illuminate\Http\Request;

class EventoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventos = evento::all();
        return view('eventos.index', compact('eventos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $direccion = direccion::all();
        $catalogo = catalogo::all();
        $cobro = cobro::all();
        $pago = tipoPago::all();
        return view('eventos.create', compact('direccion', 'catalogo', 'cobro', 'pago'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreeventoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eventos = new evento();

        $eventos->nombre = $request->get('nombre');
        $eventos->fecha = $request->get('fecha');
        $eventos->direccion_id = $request->get('direccion_id');
        $eventos->catalogo_id = $request->get('catalogo_id');
        $eventos->tipo_cobro_id = $request->get('tipo_cobro_id');
        $eventos->tipo_pago_id = $request->get('tipo_pago_id');

        $eventos->save();
        
        return redirect('/eventos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function show(evento $evento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eventos = evento::find($id);
        $direccion = direccion::all();
        $catalogo = catalogo::all();
        $cobro = cobro::all();
        $pago = tipoPago::all();
        return view('eventos.edit', compact('eventos', $eventos, 'direccion', $direccion, 'catalogo',  $catalogo, 'cobro', $cobro, 'pago', $pago));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  use Illuminate\Http\Request; $request
     * @param  \App\Models\evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $eventos = evento::find($id);

        $eventos->nombre = $request->get('nombre');
        $eventos->fecha = $request->get('fecha');
        $eventos->direccion_id = $request->get('direccion_id');
        $eventos->catalogo_id = $request->get('catalogo_id');
        $eventos->tipo_cobro_id = $request->get('tipo_cobro_id');
        $eventos->tipo_pago_id = $request->get('tipo_pago_id');

        $eventos->save();
        
        return redirect('/eventos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\evento  $evento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $evento = evento::find($id);
        $evento->delete();
        return redirect('/eventos');
    }
}
