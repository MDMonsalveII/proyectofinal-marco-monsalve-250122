<?php

namespace App\Http\Controllers;

use App\Models\parroquia;
use App\Http\Requests\StoreparroquiaRequest;
use App\Http\Requests\UpdateparroquiaRequest;

class ParroquiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreparroquiaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreparroquiaRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\parroquia  $parroquia
     * @return \Illuminate\Http\Response
     */
    public function show(parroquia $parroquia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\parroquia  $parroquia
     * @return \Illuminate\Http\Response
     */
    public function edit(parroquia $parroquia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateparroquiaRequest  $request
     * @param  \App\Models\parroquia  $parroquia
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateparroquiaRequest $request, parroquia $parroquia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\parroquia  $parroquia
     * @return \Illuminate\Http\Response
     */
    public function destroy(parroquia $parroquia)
    {
        //
    }
}
