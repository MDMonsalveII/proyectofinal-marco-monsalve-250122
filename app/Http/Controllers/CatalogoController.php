<?php

namespace App\Http\Controllers;

use App\Models\catalogo;
use Illuminate\Http\Request;

class CatalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $catalogos = catalogo::all();
       return view ('eventos.catalogo', compact('catalogos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $catalogos = catalogo::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $catalogos = new catalogo;
        $catalogos->nombre = $request->get('nombre');
        $catalogos->descripcion = $request->get('descripcion');
        $catalogos->costos = $request->get('costos');
        $catalogos->save();
        return redirect('/catalogos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\catalogo  $catalogo
     * @return \Illuminate\Http\Response
     */
    public function show(catalogo $catalogo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catalogo = catalogo::find($id);
        return view ('eventos.catalogo')->with('catalogo', $catalogo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\catalogo  $catalogo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, catalogo $catalogo)
    {
        $catalogo = new catalogo;
        $catalogo->nombre = $request->get('nombre');
        $catalogo->descripcion = $request->get('descripcion');
        $catalogo->costos = $request->get('costos');
        $catalogo->save();
        return redirect('/catalogos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\catalogo  $catalogo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $catalogo = catalogo::find($id);
        $catalogo->delete();
        return redirect('/catalogos');
    }
}
