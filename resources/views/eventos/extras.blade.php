@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8">
               <div class="py-3">
                   <div class="px-8">
                   <h1>Añadidr contenido extra</h1>
                    <form action="/extras" method="post">
                        @csrf
                        <div class="py-5 text-center row g-3">
                        <div class="form-group col-sm-6">
                            <label for="Nombre" class="form-label">Nombre del catalogo</label>
                            <input type="text" class="form-control" name="nombre">
                        </div>
                        <div class="input-group col-sm-6">
                <span class="input-group-text">Descripcion</span>
                <textarea class="form-control" name="descripcion" aria-label="With textarea"></textarea>
                         </div>
                         <div class="form-group">
                                <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                                <div class="input-group">
                                <div class="input-group-addon">$</div>
                                <input type="number" name="costos" class="form-control" id="exampleInputAmount" placeholder="Amount">
                                </div>
                            </div>
                         <div class="container">
                <div class="btn-group " role="group" aria-label="Basic outlined example">
                <button type="submit" class="btn btn-outline-primary">GUARDAR</button>
                </div>
              </div>
                        </div>
                    </form>
                   </div>
               </div>
               <table class="table table-dark table-striped mt-4">
                   <thead class="thead-dark">
                       <h2>Adiciones disponibles</h2>
                       <tr>
                           <th>#</th>
                           <th>Nombre</th>
                           <th>Descripcion</th>
                           <th>Precio</th>
                           <th scope="col">acciones</th>
                       </tr>
                   </thead>
                   <tbody>
                       @foreach($extras as $key)
                       <tr>
                       <td>{{$key->id}}</td>
                        <td>{{$key->nombre}}</td>
                        <td>{{$key->descripcion}}</td>
                        <td>{{$key->costos}}</td>
                        <td>
                            <form action="{{ route('extras.destroy',$key->id) }}" method="POST">      
              @csrf
              @method('DELETE')
          <button type="submit" class="btn btn-danger">Delete</button>
         </form>      </td>
                       </tr>
                       @endforeach
                   </tbody>
               </table>
               <a href="/" class="btn btn-primary col-sm-2" >REGRESAR</a>
        </div>
    </div>
</div>
@endsection