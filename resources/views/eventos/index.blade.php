@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="col-sm-12">
            <div class="py-3">
                <div class="px-4">
                    <div class="row justify-center">
                    <h1>listado de eventos</h1>
                    <a href="eventos/create" class="btn btn-primary col-sm-2" >CREAR</a>
<table class="table table-dark table-striped mt-4">
<thead class="thead-dark">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">nombre</th>
                            <th scope="col">fecha</th>
                            <th scope="col">direccion</th>
                            <th scope="col">catalogo</th>
                            <th scope="col">tipo de pago</th>
                            <th scope="col">acciones</th>
                            </tr>
                        </thead>
  <tbody>    
                      @foreach ($eventos as $key)
                        <tr>
                            <td>{{$key->id}}</td>
                            <td>{{$key->nombre}}</td>
                            <td>{{$key->fecha}}</td>
                            <td>{{$key->direccion_id}}</td>
                            <td>{{$key->catalogo_id}}</td>
                            <td>{{$key->tipo_pago_id}}</td>

        <td>
         <form action="{{ route('eventos.destroy',$key->id) }}" method="POST">
          <a href="/eventos/{{$key->id}}/edit" class="btn btn-info">Editar</a>         
              @csrf
              @method('DELETE')
          <button type="submit" class="btn btn-danger">Delete</button>
         </form>          
        </td>        
    </tr>
    @endforeach
  </tbody>
</table>
                </div>
            </div>
            <a href="/" class="btn btn-primary col-sm-2" >REGRESAR</a>
        </div>
    </div>
</body>
</html>